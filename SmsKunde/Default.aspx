<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Default.aspx.vb" Inherits="SmsKunde.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<asp:label id="Label1" style="Z-INDEX: 102; LEFT: 20px; POSITION: absolute; TOP: 12px" Height="30px" Font-Bold="True" Font-Size="Larger" Width="444px" runat="server">NTB Kunderegister for SMS-varsling av hastemeldinger</asp:label>
		<form id="Form1" method="post" runat="server">
			<asp:datagrid id=DataGrid1 style="Z-INDEX: 101; LEFT: 10px; POSITION: absolute; TOP: 228px" Width="833px" runat="server" DataSource="<%# DataView1 %>" DataMember="Customer" DataKeyField="ID" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Inset">
				<Columns>
					<asp:EditCommandColumn ButtonType="PushButton" UpdateText="Avbryt" CancelText="Lagre" EditText="Endre">
						<HeaderStyle Width="10px"></HeaderStyle>
					</asp:EditCommandColumn>
					<asp:BoundColumn DataField="ID" SortExpression="ID" ReadOnly="True" HeaderText="Id">
						<HeaderStyle Width="10px"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Firma" SortExpression="Firma" HeaderText="Firma">
						<HeaderStyle Width="150px"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Customer" SortExpression="Customer" HeaderText="Kunde">
						<HeaderStyle Width="150px"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Mobilphone" SortExpression="Mobilphone" HeaderText="Mobil">
						<HeaderStyle Width="70px"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Epost" SortExpression="Epost" HeaderText="E-post">
						<HeaderStyle Width="200px"></HeaderStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn SortExpression="Innenriks" HeaderText="Innenriks">
						<HeaderStyle Width="20px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:CheckBox Enabled="False" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Innenriks") %>'>
							</asp:CheckBox>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:CheckBox runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Innenriks") %>'>
							</asp:CheckBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn SortExpression="Utenriks" HeaderText="Utenriks">
						<HeaderStyle Width="20px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:CheckBox Enabled="False" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Utenriks") %>'>
							</asp:CheckBox>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:CheckBox runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Utenriks") %>'>
							</asp:CheckBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn SortExpression="Sport" HeaderText="Sport">
						<HeaderStyle Width="20px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:CheckBox Enabled="False" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Sport") %>'>
							</asp:CheckBox>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:CheckBox runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Sport") %>'>
							</asp:CheckBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="Slett" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
				</Columns>
			</asp:datagrid>
			<table style="Z-INDEX: 103; LEFT: 11px; WIDTH: 579px; POSITION: absolute; TOP: 39px; HEIGHT: 180px" cellSpacing="1" cellPadding="1" border="1">
				<TR>
					<TD bgColor="#aaaadd" colSpan="3">&nbsp;<STRONG>Ny kunde</STRONG></TD>
				</TR>
				<TR>
					<TD noWrap colSpan="1" height="10" rowSpan="1">&nbsp;Firma:</TD>
					<TD style="WIDTH: 307px" height="10"><INPUT id="Firma" style="WIDTH: 299px; HEIGHT: 22px" type="text" size="44" name="Firma" runat="server"></TD>
					<TD height="10"><INPUT id="Submit1" style="WIDTH: 135px; HEIGHT: 24px" type="submit" value="Legg til ny kunde" name="Submit1" runat="server" OnServerClick="AddCustomer_Click">
				<TR>
					<TD noWrap colSpan="1" height="10" rowSpan="1">&nbsp;Etternavn, Fornavn:</TD>
					<TD style="WIDTH: 307px" height="10"><INPUT id="Customer" style="WIDTH: 299px; HEIGHT: 22px" type="text" size="44" name="Customer" runat="server"></TD>
					<TD align="middle" height="10"></TD>
				</TR>
				<TR>
					<TD noWrap>&nbsp;Mobil nr.:</TD>
					<TD style="WIDTH: 307px"><INPUT id="Mobilphone" style="WIDTH: 299px; HEIGHT: 22px" type="text" size="44" name="Mobilphone" runat="server"></TD>
					<TD></TD>
				<TR>
				<TR>
					<TD>&nbsp;E-post:</TD>
					<TD style="WIDTH: 307px"><INPUT id="Epost" style="WIDTH: 299px; HEIGHT: 22px" type="text" size="44" name="Epost" runat="server">
					</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="1" height="10" rowSpan="1">&nbsp;SMS:</TD>
					<td style="WIDTH: 307px" height="10">&nbsp;&nbsp;Innenriks: <INPUT id="Innenriks" type="checkbox" CHECKED name="Inneriks" runat="server">&nbsp;&nbsp;&nbsp; 
						Utenriks: <INPUT id="Utenriks" type="checkbox" CHECKED name="Utenriks" runat="server">&nbsp;&nbsp;&nbsp; 
						Sport: <INPUT id="Sport" type="checkbox" name="Sport" runat="server">
					</td>
					<TD height="10"><INPUT id="SubmitWap" type="submit" value="Oppdater WapSms" name="SubmitWap" runat="server" OnServerClick="UpdateWapSms_Click" style="WIDTH: 135px; HEIGHT: 24px"></TD>
				</TR>
			</table>
			<!-- Extra table for errormessages -->
			<table id="Table2" style="Z-INDEX: 104; LEFT: 595px; WIDTH: 250px; POSITION: absolute; TOP: 38px; HEIGHT: 181px" height="181" cellSpacing="1" cellPadding="1" width="250" border="1" runat="server">
				<tr valign="center">
					<td align="middle">
						<SPAN id="Message" runat="server">
							<asp:RegularExpressionValidator id="phoneRegexVal" ControlToValidate="Mobilphone" ValidationExpression="[0-9]{8}" Display="Dynamic" Font-Name="Arial" Font-Size="11" runat="server">
							* Mobilnummer m� v�re 8 siffer, uten mellomrom!<br>
						</asp:RegularExpressionValidator></SPAN>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
