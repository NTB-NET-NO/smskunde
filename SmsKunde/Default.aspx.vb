Imports System.Data.OleDb
Imports System.IO


Public Class WebForm1
    Inherits System.Web.UI.Page

    Protected WithEvents OleDbDataAdapter1 As System.Data.OleDb.OleDbDataAdapter
    Protected WithEvents OleDbConnection1 As System.Data.OleDb.OleDbConnection
    Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents DataView1 As System.Data.DataView
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents au_idReqVal As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents au_lnameReqVal As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents au_fnameReqVal As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents au_id As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents Submit1 As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents Message As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Firma As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents Epost As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents Customer As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents Mobilphone As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents Innenriks As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents Utenriks As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents Sport As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents Table1 As System.Web.UI.WebControls.Table
    Protected WithEvents Table2 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents DataSet11 As SmsKunde.DataSet1
    Protected WithEvents OleDbSelectCommand1 As System.Data.OleDb.OleDbCommand
    Protected WithEvents OleDbInsertCommand1 As System.Data.OleDb.OleDbCommand
    Protected WithEvents OleDbUpdateCommand1 As System.Data.OleDb.OleDbCommand
    Protected WithEvents OleDbDataAdapter2 As System.Data.OleDb.OleDbDataAdapter
    Protected WithEvents OleDbSelectCommand2 As System.Data.OleDb.OleDbCommand
    Protected WithEvents DataSet21 As SmsKunde.DataSet2
    Protected WithEvents phoneRegexVal As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents SubmitWap As System.Web.UI.HtmlControls.HtmlInputButton

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.OleDbDataAdapter1 = New System.Data.OleDb.OleDbDataAdapter()
        Me.OleDbInsertCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbConnection1 = New System.Data.OleDb.OleDbConnection()
        Me.OleDbSelectCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbUpdateCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.DataView1 = New System.Data.DataView()
        Me.DataSet11 = New SmsKunde.DataSet1()
        Me.OleDbDataAdapter2 = New System.Data.OleDb.OleDbDataAdapter()
        Me.OleDbSelectCommand2 = New System.Data.OleDb.OleDbCommand()
        Me.DataSet21 = New SmsKunde.DataSet2()
        CType(Me.DataView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSet11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSet21, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'OleDbDataAdapter1
        '
        Me.OleDbDataAdapter1.InsertCommand = Me.OleDbInsertCommand1
        Me.OleDbDataAdapter1.SelectCommand = Me.OleDbSelectCommand1
        Me.OleDbDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Customer", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Customer", "Customer"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("Mobilphone", "Mobilphone"), New System.Data.Common.DataColumnMapping("Prio13Stoffgr", "Prio13Stoffgr"), New System.Data.Common.DataColumnMapping("Firma", "Firma"), New System.Data.Common.DataColumnMapping("Epost", "Epost"), New System.Data.Common.DataColumnMapping("Password", "Password")})})
        Me.OleDbDataAdapter1.UpdateCommand = Me.OleDbUpdateCommand1
        '
        'OleDbInsertCommand1
        '
        Me.OleDbInsertCommand1.CommandText = "INSERT INTO Customer(Customer, Mobilphone, Prio13Stoffgr, Firma, Epost, [Password" & _
        "]) VALUES (?, ?, ?, ?, ?, ?)"
        Me.OleDbInsertCommand1.Connection = Me.OleDbConnection1
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Customer", System.Data.OleDb.OleDbType.VarWChar, 255, "Customer"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Mobilphone", System.Data.OleDb.OleDbType.VarWChar, 50, "Mobilphone"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Prio13Stoffgr", System.Data.OleDb.OleDbType.VarWChar, 255, "Prio13Stoffgr"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Firma", System.Data.OleDb.OleDbType.VarWChar, 255, "Firma"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Epost", System.Data.OleDb.OleDbType.VarWChar, 255, "Epost"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Password", System.Data.OleDb.OleDbType.VarWChar, 6, "Password"))
        '
        'OleDbConnection1
        '
        Me.OleDbConnection1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Password="""";User ID=Admin;Data Source=C:\AccessB" & _
        "aser\WapKunder.mdb;Mode=Share Deny None;Extended Properties="""";Jet OLEDB:System " & _
        "database="""";Jet OLEDB:Registry Path="""";Jet OLEDB:Database Password="""";Jet OLEDB:" & _
        "Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Op" & _
        "s=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database Password="""";Jet " & _
        "OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Do" & _
        "n't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;" & _
        "Jet OLEDB:SFP=False"
        '
        'OleDbSelectCommand1
        '
        Me.OleDbSelectCommand1.CommandText = "SELECT Customer, ID, Mobilphone, Prio13Stoffgr, Firma, Epost, [Password] FROM Cus" & _
        "tomer WHERE (NOT (Customer = 'Alle Felt'))"
        Me.OleDbSelectCommand1.Connection = Me.OleDbConnection1
        '
        'OleDbUpdateCommand1
        '
        Me.OleDbUpdateCommand1.CommandText = "UPDATE Customer SET Customer = ?, Mobilphone = ?, Prio13Stoffgr = ?, Firma = ?, E" & _
        "post = ?, [Password] = ? WHERE (ID = ?) AND (Customer = ? OR ? IS NULL AND Custo" & _
        "mer IS NULL) AND (Epost = ? OR ? IS NULL AND Epost IS NULL) AND (Firma = ? OR ? " & _
        "IS NULL AND Firma IS NULL) AND (Mobilphone = ?) AND ([Password] = ? OR ? IS NULL" & _
        " AND [Password] IS NULL) AND (Prio13Stoffgr = ? OR ? IS NULL AND Prio13Stoffgr I" & _
        "S NULL)"
        Me.OleDbUpdateCommand1.Connection = Me.OleDbConnection1
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Customer", System.Data.OleDb.OleDbType.VarWChar, 255, "Customer"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Mobilphone", System.Data.OleDb.OleDbType.VarWChar, 50, "Mobilphone"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Prio13Stoffgr", System.Data.OleDb.OleDbType.VarWChar, 255, "Prio13Stoffgr"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Firma", System.Data.OleDb.OleDbType.VarWChar, 255, "Firma"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Epost", System.Data.OleDb.OleDbType.VarWChar, 255, "Epost"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Password", System.Data.OleDb.OleDbType.VarWChar, 6, "Password"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ID", System.Data.OleDb.OleDbType.Integer, 0, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Customer", System.Data.OleDb.OleDbType.VarWChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Customer", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Customer1", System.Data.OleDb.OleDbType.VarWChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Customer", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Epost", System.Data.OleDb.OleDbType.VarWChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Epost", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Epost1", System.Data.OleDb.OleDbType.VarWChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Epost", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Firma", System.Data.OleDb.OleDbType.VarWChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Firma", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Firma1", System.Data.OleDb.OleDbType.VarWChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Firma", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Mobilphone", System.Data.OleDb.OleDbType.VarWChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Mobilphone", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Password", System.Data.OleDb.OleDbType.VarWChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Password", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Password1", System.Data.OleDb.OleDbType.VarWChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Password", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Prio13Stoffgr", System.Data.OleDb.OleDbType.VarWChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Prio13Stoffgr", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Prio13Stoffgr1", System.Data.OleDb.OleDbType.VarWChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Prio13Stoffgr", System.Data.DataRowVersion.Original, Nothing))
        '
        'DataView1
        '
        Me.DataView1.Table = Me.DataSet11.Customer
        '
        'DataSet11
        '
        Me.DataSet11.DataSetName = "DataSet1"
        Me.DataSet11.Locale = New System.Globalization.CultureInfo("nb-NO")
        Me.DataSet11.Namespace = "http://www.tempuri.org/DataSet1.xsd"
        '
        'OleDbDataAdapter2
        '
        Me.OleDbDataAdapter2.SelectCommand = Me.OleDbSelectCommand2
        Me.OleDbDataAdapter2.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Customer", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("Customer", "Customer"), New System.Data.Common.DataColumnMapping("Mobilphone", "Mobilphone"), New System.Data.Common.DataColumnMapping("Password", "Password"), New System.Data.Common.DataColumnMapping("SMS", "SMS"), New System.Data.Common.DataColumnMapping("WAP", "WAP"), New System.Data.Common.DataColumnMapping("Prio13Stoffgr", "Prio13Stoffgr"), New System.Data.Common.DataColumnMapping("blnSniffertxt", "blnSniffertxt"), New System.Data.Common.DataColumnMapping("NTBSniffertxt", "NTBSniffertxt"), New System.Data.Common.DataColumnMapping("NTBStoffgrupper", "NTBStoffgrupper"), New System.Data.Common.DataColumnMapping("NTBOmraader", "NTBOmraader"), New System.Data.Common.DataColumnMapping("NTBFylker", "NTBFylker"), New System.Data.Common.DataColumnMapping("NTBHovedKategorier", "NTBHovedKategorier"), New System.Data.Common.DataColumnMapping("NTBUnderKatSport", "NTBUnderKatSport"), New System.Data.Common.DataColumnMapping("NTBUnderKatOkonomi", "NTBUnderKatOkonomi"), New System.Data.Common.DataColumnMapping("NTBUnderKatKuriosa", "NTBUnderKatKuriosa"), New System.Data.Common.DataColumnMapping("NTBUnderKatKultur", "NTBUnderKatKultur"), New System.Data.Common.DataColumnMapping("Adresse", "Adresse"), New System.Data.Common.DataColumnMapping("Stilling", "Stilling"), New System.Data.Common.DataColumnMapping("Firma", "Firma"), New System.Data.Common.DataColumnMapping("PostNr", "PostNr"), New System.Data.Common.DataColumnMapping("Poststed", "Poststed"), New System.Data.Common.DataColumnMapping("Epost", "Epost"), New System.Data.Common.DataColumnMapping("DatoOpprettet", "DatoOpprettet"), New System.Data.Common.DataColumnMapping("AntallEndringer", "AntallEndringer"), New System.Data.Common.DataColumnMapping("REMOTE_ADDR", "REMOTE_ADDR"), New System.Data.Common.DataColumnMapping("DatoSistEndret", "DatoSistEndret")})})
        '
        'OleDbSelectCommand2
        '
        Me.OleDbSelectCommand2.CommandText = "SELECT Adresse, AntallEndringer, blnSniffertxt, Customer, DatoOpprettet, DatoSist" & _
        "Endret, Epost, Firma, ID, Mobilphone, NTBFylker, NTBHovedKategorier, NTBOmraader" & _
        ", NTBSniffertxt, NTBStoffgrupper, NTBUnderKatKultur, NTBUnderKatKuriosa, NTBUnde" & _
        "rKatOkonomi, NTBUnderKatSport, [Password], PostNr, Poststed, Prio13Stoffgr, REMO" & _
        "TE_ADDR, SMS, Stilling, WAP FROM Customer"
        Me.OleDbSelectCommand2.Connection = Me.OleDbConnection1
        '
        'DataSet21
        '
        Me.DataSet21.DataSetName = "DataSet2"
        Me.DataSet21.Locale = New System.Globalization.CultureInfo("nb-NO")
        Me.DataSet21.Namespace = "http://www.tempuri.org/DataSet2.xsd"
        CType(Me.DataView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSet11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSet21, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        OleDbConnection1.ConnectionString = Session("WapKunderDB")
        If Session("mydatset") Is Nothing Then
            OleDbDataAdapter1.Fill(DataSet11)
            Session("mydataset") = DataSet11
        Else
            DataSet11 = CType(Session("mydatset"), DataSet1)
        End If

        SetCheckboxSMS()

        If Not (Session("previousSort") Is Nothing) Then
            DataView1.Sort = Session("LastSort")
        End If

        If Not IsPostBack Then
            DataGrid1.DataBind()
        End If

    End Sub

    Sub SetCheckboxSMS()

        Dim row As DataSet1.CustomerRow
        Dim customerPrio13Stoffgr As String

        For Each row In DataSet11.Customer.Rows
            customerPrio13Stoffgr = row.Prio13Stoffgr
            If customerPrio13Stoffgr.IndexOf("Innenriks;") > -1 Then
                row.Innenriks = True
            Else
                row.Innenriks = False
            End If

            If customerPrio13Stoffgr.IndexOf("Utenriks;") > -1 Then
                row.Utenriks = True
            Else
                row.Utenriks = False
            End If

            If customerPrio13Stoffgr.IndexOf("Sport;") > -1 Then
                row.Sport = True
            Else
                row.Sport = False
            End If
        Next
    End Sub

    Private Sub DataGrid1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
        DataGrid1.EditItemIndex = e.Item.ItemIndex
        DataGrid1.DataBind()
    End Sub

    Private Sub DataGrid1_CancelCancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.UpdateCommand
        ' NB! Cancel and Update buttans have been Swapped!
        DataGrid1.EditItemIndex = -1
        DataGrid1.DataBind()
    End Sub

    Private Sub DataGrid1_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.CancelCommand
        ' NB! Cancel and Update buttons have been Swapped!

        ' Gets the value of the key field of the row being updated
        Dim key As String = DataGrid1.DataKeys(e.Item.ItemIndex).ToString()

        ' 2. Get the changed values out of the DataGrid row:
        Dim customerFirma, customerName, customerMobilphone, customerEpost As String
        Dim customerPrio13Stoffgr As String
        Dim tmpInnenriks, tmpUtenriks, tmpSport As Boolean
        Dim customer
        Dim tb As TextBox
        Dim cb As CheckBox

        tb = CType(e.Item.Cells(2).Controls(0), TextBox)
        customerFirma = tb.Text

        tb = CType(e.Item.Cells(3).Controls(0), TextBox)
        customerName = tb.Text

        tb = CType(e.Item.Cells(4).Controls(0), TextBox)
        customerMobilphone = tb.Text

        tb = CType(e.Item.Cells(5).Controls(0), TextBox)
        customerEpost = tb.Text

        'Dim ContractCheckBox As CheckBox
        'ContractCheckBox = e.Item.FindControl("edit_Contract")

        cb = e.Item.Cells(6).Controls(1)
        tmpInnenriks = cb.Checked
        cb = e.Item.Cells(7).Controls(1)
        tmpUtenriks = cb.Checked
        cb = e.Item.Cells(8).Controls(1)
        tmpSport = cb.Checked

        If tmpInnenriks Then
            customerPrio13Stoffgr = "Innenriks;"
        End If

        If tmpUtenriks Then
            customerPrio13Stoffgr &= "Utenriks;"
        End If

        If tmpSport Then
            customerPrio13Stoffgr &= "Sport;"
        End If
        'NY KODE 24.06.2002 RoV:
        If customerPrio13Stoffgr <> "" Then
            customerPrio13Stoffgr &= "Priv-til-red;"
        End If

        ' 3. Find the corresponding row in the data table
        Dim r As DataSet1.CustomerRow
        r = Me.DataSet11.Customer.FindByID(key)

        ' Finds the row in the dataset table that matches the 
        ' one the user updated in the grid. This example uses a 
        ' special Find method defined for the typed dataset, which
        ' returns a reference to the row.
        r.Customer = customerName
        r.Firma = customerFirma
        r.Mobilphone = customerMobilphone.Replace(" ", "")
        r.Epost = customerEpost
        r.Prio13Stoffgr = customerPrio13Stoffgr
        r.Innenriks = tmpInnenriks
        r.Utenriks = tmpUtenriks
        r.Sport = tmpSport

        ' Updates the dataset table.
        Me.OleDbDataAdapter1.Update(DataSet11)
        Session("mydataset") = Me.DataSet11
        'DataGrid1.DataBind()

        ' Takes the DataGrid row out of editing mode
        DataGrid1.EditItemIndex = -1

        ' Refreshes the grid
        DataGrid1.DataBind()
    End Sub

    Private Sub DataGrid1_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DataGrid1.SortCommand
        ' Sorting collumns alternately: ASCending or DESCending

        Dim previousSort As String

        If Session("previousSort") Is Nothing Then
            ' First time sort is used in a session
            previousSort = ""
        Else
            previousSort = Session("previousSort")
        End If

        If e.SortExpression = previousSort And Not previousSort.EndsWith("DESC") Then
            previousSort &= " DESC"
            DataView1.Sort = previousSort
        Else
            previousSort = e.SortExpression
            DataView1.Sort = previousSort
        End If

        ' Save this sort until next time
        Session("previousSort") = previousSort

        DataGrid1.EditItemIndex = -1
        DataGrid1.DataBind()

    End Sub

    Sub AddCustomer_Click(ByVal Sender As Object, ByVal E As EventArgs)
        Message.InnerHtml = ""

        If (Page.IsValid) Then

            Me.OleDbInsertCommand1.Parameters("Firma").Value = Firma.Value
            Me.OleDbInsertCommand1.Parameters("Customer").Value = Customer.Value
            Me.OleDbInsertCommand1.Parameters("Mobilphone").Value = Mobilphone.Value.Replace(" ", "")
            Me.OleDbInsertCommand1.Parameters("Epost").Value = Epost.Value
            Me.OleDbInsertCommand1.Parameters("Password").Value = "1111"

            Dim customerPrio13Stoffgr As String = ""
            If Innenriks.Checked Then
                customerPrio13Stoffgr = "Innenriks;"
            End If
            If Utenriks.Checked Then
                customerPrio13Stoffgr &= "Utenriks;"
            End If
            If Sport.Checked Then
                customerPrio13Stoffgr &= "Sport;"
            End If
            If customerPrio13Stoffgr <> "" Then
                Me.OleDbInsertCommand1.Parameters("Prio13Stoffgr").Value = customerPrio13Stoffgr
            End If

            Me.OleDbInsertCommand1.Connection.Open()

            Try
                Me.OleDbInsertCommand1.ExecuteNonQuery()
                Message.InnerHtml = "<b>Ny kunde er lagt til. (Se nederst i lista)</b>"
                Message.Style("color") = "normal"
                Firma.Value = ""
                Customer.Value = ""
                Mobilphone.Value = ""
                Epost.Value = ""
                Innenriks.Checked = True
                Utenriks.Checked = True
                Sport.Checked = False
            Catch Exp As OleDbException
                If Exp.ErrorCode = -2147467259 Then
                    Message.InnerHtml = "FEIL: Mobilnummer finnes fra f�r!"
                Else
                    'Message.InnerHtml = "ERROR: Could not add record, please ensure the fields are correctly filled out"
                    Message.InnerHtml = "FEIL: " & Exp.ErrorCode & "<br/>" & Exp.Message
                End If
                Message.Style("color") = "red"
            End Try

            Me.OleDbInsertCommand1.Connection.Close()

            OleDbDataAdapter1.Fill(DataSet11)

            SetCheckboxSMS()
            If Not (Session("previousSort") Is Nothing) Then
                DataView1.Sort = Session("LastSort")
            End If
            Session("mydataset") = DataSet11
            DataGrid1.DataBind()
        End If

    End Sub

    Sub UpdateWapSms_Click(ByVal Sender As Object, ByVal E As EventArgs)
        '*** Lager fullstendig Customer fil for SmsWap Service:
        OleDbDataAdapter2.Fill(DataSet21)

        Dim strSlutt, strFullName, strMobilphone, strblnSMS, strblnWAP As String
        Dim strPrio13Stoffgr, strblnSniffertxt, strNTBSniffertxt, strNTBStoffgrupper As String
        Dim strNTBOmraader, strNTBFylker, strNTBHovedKategorier, strNTBUnderKatSport, strNTBUnderKatOkonomi As String
        Dim strNTBUnderKatKuriosa, strNTBUnderKatKultur As String

        Dim row As DataSet2.CustomerRow
        Dim customerPrio13Stoffgr As String

        Dim strFilNavn As String = Session("WAPCustomerFile")
        Dim objLoggFil As TextWriter = File.CreateText(strFilNavn)

        For Each row In DataSet21.Customer.Rows
            With row
                strSlutt = ">"
                strFullName = "<Customer=" & .Customer & strSlutt
                strMobilphone = "<Mobilphone=" & .Mobilphone & strSlutt
                If .SMS = "True" Then
                    strblnSMS = "<SMS=Sann" & strSlutt
                Else
                    strblnSMS = "<SMS=Usann" & strSlutt
                End If

                If .WAP = "True" Then
                    strblnWAP = "<WAP=Sann" & strSlutt
                Else
                    strblnWAP = "<WAP=Usann" & strSlutt
                End If

                strPrio13Stoffgr = "<Prio13Stoffgr=" & Trim(.Prio13Stoffgr) & strSlutt
                If Trim(.NTBSniffertxt) <> "" Then
                    strblnSniffertxt = "<blnSniffertxt=Sann" & strSlutt
                Else
                    strblnSniffertxt = "<blnSniffertxt=Usann" & strSlutt
                End If

                If Right(.NTBSniffertxt, 1) <> ";" And Trim(.NTBSniffertxt) <> "" Then
                    strNTBSniffertxt = "<NTBSniffertxt=" & .NTBSniffertxt & ";" & strSlutt
                Else
                    strNTBSniffertxt = "<NTBSniffertxt=" & .NTBSniffertxt & strSlutt
                End If

                strNTBSniffertxt = Replace(strNTBSniffertxt, "; ", ";")
                strNTBSniffertxt = Replace(strNTBSniffertxt, " ;", ";")

                strNTBStoffgrupper = "<NTBStoffgrupper=" & .NTBStoffgrupper & strSlutt
                strNTBOmraader = "<NTBOmraader=" & .NTBOmraader & strSlutt
                strNTBFylker = "<NTBFylker=" & .NTBFylker & strSlutt
                strNTBHovedKategorier = "<NTBHovedKategorier=" & .NTBHovedKategorier & strSlutt
                strNTBUnderKatSport = "<NTBUnderKatSport=" & .NTBUnderKatSport & strSlutt
                strNTBUnderKatOkonomi = "<NTBUnderKatOkonomi=" & .NTBUnderKatOkonomi & strSlutt
                strNTBUnderKatKuriosa = "<NTBUnderKatKuriosa=" & .NTBUnderKatKuriosa & strSlutt
                strNTBUnderKatKultur = "<NTBUnderKatKultur=" & .NTBUnderKatKultur & strSlutt

                strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kongestoff;", "Kuriosa;")
                strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kuriosa;", "Kuriosa2;", 1, 1)
                strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kuriosa;", "")
                strNTBHovedKategorier = Replace(strNTBHovedKategorier, "Kuriosa2;", "Kuriosa;")

                strNTBOmraader = Replace(strNTBOmraader, "Norge;", "Norge2;", 1, 1)
                strNTBOmraader = Replace(strNTBOmraader, "Norge;", "")
                strNTBOmraader = Replace(strNTBOmraader, "Norge2;", "Norge;")

                strNTBHovedKategorier = Replace(strNTBHovedKategorier, "AlleHov;", "")
                strNTBOmraader = Replace(strNTBOmraader, "AlleOmr;", "")
                strNTBFylker = Replace(strNTBFylker, "AlleFyl;", "")
                strNTBUnderKatKultur = Replace(strNTBUnderKatKultur, "AlleKul;", "")
                strNTBUnderKatOkonomi = Replace(strNTBUnderKatOkonomi, "AlleOko;", "")
                strNTBUnderKatSport = Replace(strNTBUnderKatSport, "AlleSpo;", "")


            End With
            objLoggFil.WriteLine(strFullName & strMobilphone & strblnSMS & strblnWAP & strPrio13Stoffgr & strblnSniffertxt & strNTBSniffertxt & strNTBStoffgrupper & strNTBOmraader & strNTBFylker & strNTBHovedKategorier & strNTBUnderKatSport & strNTBUnderKatOkonomi & strNTBUnderKatKuriosa & strNTBUnderKatKultur)

        Next
        DataSet21.WriteXml(strFilNavn & ".xml")
        DataSet21.WriteXmlSchema(strFilNavn & ".xsd")
        objLoggFil.Close()

        Message.InnerHtml = "<b>WapSms service er n� oppdatert med alle tillegg og endringer</b>"
        Message.Style("color") = "normal"

    End Sub

    Private Sub DataGrid1_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.DeleteCommand
        Dim intKey As Integer = DataGrid1.DataKeys(e.Item.ItemIndex)

        Dim MyCommand As OleDbCommand
        Dim DeleteCmd As String = "DELETE from Customer WHERE ID = @Id"

        MyCommand = New OleDbCommand(DeleteCmd, OleDbConnection1)
        MyCommand.Parameters.Add(New OleDbParameter("@Id", OleDbType.Integer, 10))
        MyCommand.Parameters("@Id").Value = intKey
        MyCommand.Connection.Open()

        Try
            MyCommand.ExecuteNonQuery()
            Dim strNavn As String = e.Item.Cells.Item(3).Text
            Message.InnerHtml = "<b>" & intKey & ": " & strNavn & ", er slettet!</b>"
            Message.Style("color") = "normal"
            'e.Item.Style("color") = "red"
        Catch Exc As OleDbException
            Message.InnerHtml = "ERROR: Could not delete record"
            Message.Style("color") = "red"
        End Try

        MyCommand.Connection.Close()

        OleDbDataAdapter1.Fill(DataSet11)
        SetCheckboxSMS()
        Session("mydataset") = DataSet11
        DataGrid1.DataBind()

    End Sub

End Class
